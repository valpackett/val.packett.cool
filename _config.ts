import lume from "lume/mod.ts";
import nunjucks from "lume/plugins/nunjucks.ts";
import date from "lume/plugins/date.ts";
import read_info from "lume/plugins/reading_info.ts";
import code_highlight from "lume/plugins/code_highlight.ts";
import createSlugifier from "lume/core/slugifier.ts";
import { encodeBase64 } from "jsr:@std/encoding";
import { default as anchor } from "npm:markdown-it-anchor@9.2.0";
import { default as ins } from "npm:markdown-it-ins@4.0.0";

const site = lume({}, {
  markdown: {
    options: {
      typographer: true,
    },
    useDefaultPlugins: true,
    plugins: [[anchor, {
      permalink: anchor.permalink.headerLink({ safariReaderFix: true }),
      slugify: createSlugifier(),
    }], ins],
  },
});

site.helper(
  "asset",
  async (name) =>
    `/x/${name}?v=${
      encodeBase64(
        (await crypto.subtle.digest(
          "SHA-256",
          await Deno.readFile(site.root(`x/${name}`)),
        )).slice(0, 8),
      )
    }`,
  { type: "tag", async: true },
);

site.filter("take", (x, n) => x.slice(0, n));
site.filter("drop", (x, n) => x.slice(n, x.length));

site
  .copy("x")
  .copy([".jpg", ".png", ".webp", ".webm", ".avif", ".jxl", ".svg", ".pdf", ".json", ".jws"])
  .use(nunjucks())
  .use(date())
  .use(read_info())
  .use(code_highlight());

export default site;
