---
layout: layouts/info.njk
title: Colophon
---

Here you can find various behind-the-scenes details about this website:

## Inspiration

Many layout ideas on the home page were inspired by the website of my pal [Sage](https://www.wavebeem.com/).
They also drew my profile picture :)

## Hosting

This website is currently hosted by [Bunny.net](https://bunny.net?ref=sqbajor5vc).
I use [Njalla](https://njal.la/) for actually private domain registration
and [Migadu](https://www.migadu.com/) for email.

No third party embeds except [shields.io](https://shields.io/) dynamic SVG images ~~have been hurt~~ are used on this website.
This is even enforced with `Content-Security-Policy`.
And yes, this means there are no analytics whatsoever.

## Content management

This website is generated with [Lume](https://lume.land/), a static site generator that runs on [Deno](https://deno.land/).

The source is available [right there on Codeberg](https://codeberg.org/valpackett/val.packett.cool), you can take a look!

## Typefaces

- [Chivo](https://www.omnibus-type.com/fonts/chivo/) 🇦🇷 for body text (and Chivo Mono for code),
- [Silkscreen](https://kottke.org/plus/type/silkscreen/) for the tiny nostalgic badges at the bottom of the page.

## Icons

Brand icons are from [Simple Icons](https://simpleicons.org/)
while others like "email" are from [Font Awesome](https://fontawesome.com/).

Exceptions:

- the Lobste.rs icon is based on [the official SVG](https://github.com/lobsters/lobsters/blob/master/app/assets/images/logo-bw.svg),
- same with the Treehouse one.

## Helpful tools and tricks

- [Wavy Shapes generator](https://css-generators.com/wavy-shapes/)
- [fffuel.co](https://fffuel.co/) various generators
- [SVG to Data URI encoder](https://heyallan.github.io/svg-to-data-uri/)
- [fonttools](https://github.com/fonttools/fonttools) (`pyftsubset`)
- [Building a combined CSS-aspect-ratio-grid](https://9elements.com/blog/building-a-combined-css-aspect-ratio-grid/)

## Browser support

This website is [best viewed in Firefox](https://www.mozilla.org/en-US/firefox/new/)!
But all reasonably popular and recent browsers should be supported.
If you notice a major issue in any browser, contact me using any of the methods listed on [the home page](/#elsewhere).
