---
title: "Introducing TiddlyPWA: putting TiddlyWiki on modern web app steroids"
desc: Oops, I think I just turned the legendary personal wiki system into an offline-first, installable, encrypted, synchronized Progressive Web App
---

{% import "media.njk" as media %}

*tl;dr: go play with the new thing [there](https://tiddly.packett.cool/), report issues [there](https://codeberg.org/valpackett/tiddlypwa/issues) and support me [there](https://www.patreon.com/valpackett) :3 but please do read the full story below!*

[TiddlyWiki](https://tiddlywiki.com/) is quite a famous note-taking system.
Describing itself as "a non-linear personal web notebook", it's actually quite a bit more than that.
Thanks to a plugin system, it's basically an app platform, and plenty of [cool stuff](https://thaddeusjiang.github.io/Projectify/) has been made for it.

And it exists as a unique thing: a self-contained web application—a single HTML file with both the app and the content—that you can take around on a USB flash drive.
Oh wait… who even uses those anymore for anything other than OS installers and bringing files to a print shop?

So of course, a bunch of storage solutions have sprung up.
There's an official node.js based server mode where it becomes more like a traditional web app,
there are browser extensions and even modules inside of TW itself for saving the updated single file to a server,
there are mobile and desktop apps that try to combine that saving with some kind of offline support,
there are people using a file sync solution like Syncthing – I've even heard of some people using Syncthing with the server version,
syncing the `.tid` files between devices and *running the server in [Termux](https://termux.dev/en/) on their phones* to access them on mobile.
Oof. While I'm nerdy enough to *be able* to do that, I'm also enough of a spoiled basic consumer to know that that's not the user experience *I want*.

What I want is something that works like *an app*. Fully working offline, syncing *efficiently* (not by POSTing a multi-megabyte HTML file), quickly and reliably when online.
And with client-side encryption, because there's just *no reason* to let the server side read the contents here.

There has actually been one good attempt at bringing this kind of sync to TiddlyWiki: [NoteSelf](https://noteself.org/), which integrated [PouchDB](https://pouchdb.com/) for storage.
I liked the sound of it, but in practice it wasn't up to my standards.
It's a heavyweight modification of TiddlyWiki that doesn't keep up with latest core updates, PouchDB/CouchDB feel a bit heavy themselves, there's no encryption, and the offline support is just "run it from your hard drive".

So… this is where I come in.
Last year, looking to try TiddlyWiki once again, looking through the options and getting frustrated with the aforementioned everything, one thing inspired me.
I stumbled upon [a basic IndexedDB plugin](https://github.com/Jermolene/TiddlyWiki5/issues/3420) which made me realize that there is an API for storage backends inside TiddlyWiki.
I realized that there's no need for core modifications, that I could just start with this and—knowing what I know about the web platform—take it to the next level.
Make a plugin that combines IndexedDB, encryption, a sync engine (with a custom super-lightweight server counterpart for it), and adds a Service Worker and a Web Manifest
to turn TW into a [Progressive Web App](https://web.dev/what-are-pwas/) that works offline and installs on mobile with "add to home screen".
That was a whole Vision. And I knew it had to be done. It just had to exist. So I got to work, putting aside my typical FreeBSD system-level work to get back to good old web dev.

Now, a whole year after that inspiring moment, having gone through huge personal life changes and a reverse putting-aside in favor of my other work again…
it's finally ready for the public to see.

So.

[Here it is.](https://tiddly.packett.cool/).

{{ media.video({
  width: 1920,
  height: 1080,
  muted: true,
  poster: "data:image/avif;base64,AAAAIGZ0eXBhdmlmAAAAAGF2aWZtaWYxbWlhZk1BMUIAAAGNbWV0YQAAAAAAAAAoaGRscgAAAAAAAAAAcGljdAAAAAAAAAAAAAAAAGxpYmF2aWYAAAAADnBpdG0AAAAAAAEAAAAsaWxvYwAAAABEAAACAAEAAAABAAACLQAAEIgAAgAAAAEAAAG1AAAAeAAAAEJpaW5mAAAAAAACAAAAGmluZmUCAAAAAAEAAGF2MDFDb2xvcgAAAAAaaW5mZQIAAAAAAgAAYXYwMUFscGhhAAAAABppcmVmAAAAAAAAAA5hdXhsAAIAAQABAAAAw2lwcnAAAACdaXBjbwAAABRpc3BlAAAAAAAABQAAAALQAAAAEHBpeGkAAAAAAwgICAAAAAxhdjFDgQUMAAAAABNjb2xybmNseAACAAIABoAAAAAOcGl4aQAAAAABCAAAAAxhdjFDgQUcAAAAADhhdXhDAAAAAHVybjptcGVnOm1wZWdCOmNpY3A6c3lzdGVtczphdXhpbGlhcnk6YWxwaGEAAAAAHmlwbWEAAAAAAAAAAgABBAECgwQAAgQBBYYHAAARCG1kYXQSAAoHGWpn/Z/KoDJrE/wEIFYAWRoZFFM8pObBcj0LotyhWLs+CPamzgDO3Y2pbLeJPG5YJuBoUVuN1OPoVzJOLTUhzPg2C6Nom6DlJb0gcr14ky8f3wG5kwngoioo6NiZj3NEgUXgeUXVzu3zvkinGLoHG3/rIaASAAoKGWpn/Z/JAgIGhDL3IEzUQUEI8lJy/UeEuJt/8SpGcGqR8FEt5otYnI/Zchm2rtKmQUeLToLU9FD5KOjOZTeuGpMH6B8prGVKcgeOqAKXt0AaEuSjju3graECLqdKyBoj2nB2fOxNdvALNeKU3R29YvvaEAzBhFafzOxdKBaIYgFRGctEFDozLkqgQ1/++6r6jEqxX///nys5GURrp/wfe1BsRjzEkZEf/vnJYIPmRv//6b7w6LxlTuaviMZaW7hJs/dXIoOHEoC65jQ2qfNrkpX0QHR/7VKYijqS/fL6Q+zBPjh3Rtl0UaZU2ccwCjiaf6L1gGGzMzmeUAAFxO3X9d21wFVJXqw15C2316+rsNpMQBwzf7dZ04sqWFY6zZ+xBxB22NH5yvJw6OksYiZa4P5X0LCQWLzOe3ffedy6GCk6n5tiaX0oYOjhb/orCApA41xY0VHjmLxRsGVd/2Z7lJggyG8BovOv7OxFaf6um2hKSniQSKkRdArVm6dkN/y2qzzI+As5aS4vl1sNJvrYAz/Lh6rar1YYNFBl0iQSEeUWeaw31uSCxuxFxJBypVqID2zLxyZJyGD197C258gt+f1jz/3qD9ES+xu92q/AKvoEx5q5D7id6/4v+/pg8RlU/nC84IPbqhhUn/W28/Ho0AIbplOlQDUzdzr7jE41/U5g+t7HPkV3t/a/Jp0nMHIFUSx0456WaImpCq2+CKSFtUjbSZU7ETwvMV8uY8RnIR0zo2IsKZJtkBI7Ew/U+KJ+WxFcwO2KjTY/DqziV06GWvhoxfJCT5wej9g7I8BSXIYbzvt7TEnXN/7Jxgjdh75p9L1wbh/Zraot+WcbUP58Gz7tT8NjTwTSg3UqFGaRdvTnW7D7ait8KlC5yG/4ZvEB8s4umT0WAKoCPRb359686IJlB/jx8GVra9lrDfx+A0mpL5P1FxemmDNapxGI1IxOXUEUuZobVKB6RWzKOT8k59AOuZZ1p4aRAsI/WxRWfmfs5N9PyupxVJfe5SeFVUiOX4sDks3rFV1X617CGhM7xWkqIiXBqvmir0tP9D4ASrry2tUHZzHkOJz5Mf6POcfnVnWb2J33LCUd1zCtYUD+s45bM1ucLb28Y6WDAifn7IPriT3ecwIeFVXEcl176osST17bjroWz0kWBegbbjXt1bcMxLs4MWFpjOcuT0IP/3IXIZ/ZlGK4p4F8z0jQn+5rOQGnhuMCGRFnfzAYAtfqYX0kFBWRmPKEjRimtIr7TmsRdYUuva6csAwbxUg7/z+RA5SMY32q0lmdPZ5XvQJxy7GkGwmVMCTnh6e+t8ZgX+/tk2V2VsvhY3jv6nrTaxCbmMuiH4hG2H8Jj2FBaDaNEJE5W2rTtChVSfulKXzZc5MzaS3leOr4L5Rzm6+aWy50iB476ffzgqdh4Ls30L5ywWDkr3Kx5HAgcUeppb+DliTNDrtp4Ze+m4lorbAQ6qzET2WJLNiJYZ+pJt8wWJe/qpWVTnCZx4MVlqzNyLedNNzpbPjnuzVbYkog2vNeXBvaUOrSd3qG7Jc8nAExzu53SSMeg03pf/u8xEPVp0Svo5sQspDjHY2woDbxyFcDisav7p9cwiELCv+9SK5lEOSJmCDEqJajeR9q56BJOnOnyDmjWX6wp7CskETN0Rs16AajjnF0wAfYx88RvdwfKu6j+/WWNr+nN1REoPFGg3DiahMtt01jskZsyH0WKd0bwkc6XutWPmd6przV63wZiIiLVbebRNAPfX8z5eXQZ8R/7rWx5Xc9xcVJOaoCG85jQigwGYvdWEt1L5d9aBcdfSmoKwkVaXRE+MQ3IU7CT/1EI6l8kHDACmA3ga7mnPPomFPSK3mKL4ORF7KRfGAR+miQviIrgC8mG1UzUZDMjWlXrmQSOlvk6fkEgFaW5geWd+lWnYzxRoIaGjrfZUq8LdbO19w8ngpKw2Bkt0VxS8aKNY/NT+mQqgGpabzgllfV0hdKZGiiBIZiUii5pMxLuoTU+Ms0AS/EYg/u1pU0T8MV/zi6SRjwfik+/68fi0dU032CCJoKSgMOae/ibxNgU58raDkTAZ56lteB2sogxeu2Sfrhh+sezNj08KVZARqTR9dp31PIQ6LsT+neLefoCFrlLM+c+BKxsJWXYh/Ab5wlyP86cf/evWrfPzqDgSBGB2cTGJ9VTqs1pljkAa0X3QeWSB5zBV4VgLELTna03U70szpuR4vdFxrcWlhhaXIv/o5itl5Pp/Vojo/nLKcbvJOphhZjRDMWieFZ9iw0oIJz40KJilINJbmprVY4n1jbFtXWLITZs9WzbEhHNyavKphkB5SEp7sbAXqfC/aQqO1/KWK9kLIIOHr7l+u5KJVLoo+fzs08NxfJYjVQBkPMnIwTERPGYRrkVeeS450WmSMCFHijjB7JOz7jcm2aSxuSmRWq1dEjhSumFegOHh8avmMm2sVDeKrSb2gTvwoWoFKTcdBtAiczF6rR9EP9tFWfIv/54/QES7/iGWIRTX4hPWSDF8EJZfRJoZ9/XcZeh5zvShw0CR48di3pFnm1JBG/+ZsvxPqt6S1G0MV2cDmLBpTnPvK6R2pzEwqV9LLN3jTY/vti37uqSwQTgiUEqkQvVo+a3YeUQPjlBfCuYJSdN801hQn223tiIAmN0xAM/P5AyrzUHAYYxdT9Nt/skfrdM/1Tze7P+JNpQDcH0k8hssEJEl4SQ0M3+I9PIvX+k/7xfqiX5ocra+dIX1xYbnHUvTOQorjl+0xO9Eor2shn3pD/hGTYPopGZbTcPIfBZ87upXOIw2AE6+KweDA7/Ld7EwPZ4TartCiYOT4ZNq8zBz+SVRmtK0RfJb8uY7KDfckU+Jy6OjOU9qkcK/xpJKGJv0IOVLVaeDb1u0LCtpCqvZvJwWj/XyCS+O8mqcWMHesAPrnFGzJm7X0hz0ZAWUIFZU38E2id/4RGS7uSWtfq9/79IMzeCKji5h8rvhwxNIUdvX2JPSMJQaqao1LaxcXfT4K0+QtJKB8CiTIjd0P+5lOyKLQo169s//jhh4hTIrvuJQdu5loWbx7WMCvjpPLj7GCaTMiiZ72wkp7urChQPRIwCVSm653f/8O8W3Htm+xYtgRYloQSdHMfb55Z1fSJzwwi7H5SmK/9XRW39QZrNvg0ATPEdVxn3GSifoVJUjDroqcYYGxHWHUQKs0zzo6v/FSfO/UzGXRFyIp2xL6BhknxeFBkfIIdxZBlKahKQStWxn7nB5HeB/WAAiaGBJW2BKr5UqoFUgolAOut57R3sgJjaDyBvlEXYCQfNzM4M5iLGaGnoN6Kd2yxCEcMoaaEO94B1BeY+tSpqyDPE79X3O0Et3tqkgCEI1ouJohZevS7DurrHQ8MjY0v5g2OJ63Ydla1oETQFn0lwS0pqmDBQCCg03Na7nB0T3z2xSJfe/Is42nlzAtr8JMamhYHMc/5zYfZo31KBVnG2/VKKZoZ25tFQLxm7vOjlJCTRWvensI7037MvEmgnVNhZLyzIngf9nhit+UORKcCu6ZWt0+RkmFHWYl71fyBqUbakh6DVGNXTZEs/25jllG+JnITM3u6H/+I8rWXGVVEfP2GuopYXRBHvH31C7+EGctYRfflDGYGy0+zJyQXBgZmZlnTCEoiftl8sJzqU/vRXexGetjwCn+m9Qq7K37ZObpnGiC97ifMNo/4jAujbZwkxuS//88wXZPiPh8lvyOC1ggJh/rfkDWjMvdQsfLP51Ah1bmZeA3hVcr+jO3uNUVIXcZgZZM6zaFIPpe7fY+JuRdzu7S6IIw3oUYTrTd9iN4KaQmFUgL7M7ECehtYuh0bRiFlLR+kQbSq+K7uThs4Cx8sbQ11aCM0617CFiX0DmBl/uEC6esdRWqRZWTIoru66WArhmvwKKXTxKB7NQFMHgZCDPx72/yVrUzQWbEpmXuiEMeynvoHYc9Bs5kj/QRHaw1MdJq5S4StzvSdHTfWEewx6odtTfZgfPAnn9FjNzEAAh67zfjr4Ly9NUI7ObjAsukhLfvmek3WizDjHGzAFyzQVyzHG3jX2Jbi/5+sCukZm46M7TFSleiwfsHhdQdyQEu02Xb+IypOrY565KUN/pW7Y5UJbs9iTlv26tmzgn2n0hCQjnTDC3ngnMOx7B3BkKrFFyC7lvhyaWtPyVxhvtPHJmK2III9SF1rT4T9nkxs3ba4QGCU7tjPw0Dq0h5kVUpLQhb314J1LCrjqC4wDgdJw5GksfcafVkH9KMrPQZYGOlGYg1a6VV5d3mgSzad+rhXjks/BKTncKAZV06Nc89LVQW83jK5tobXCAw/xuRB2WDmGF+wVWPCmK4E2lroXMO43j7sE4QEmHu5z0o5U/xPwh1iLC48c0WLFRYymN2H6EpbFbIwFcwo1VJ6PCHd47ykH7sH5blaSNoQP7HHbBVhDEmOffmO9ZW3+sHXkGJ9lwXpmyLpwDDhTwj+mbQBBa+qCjDgatWQm/l+HhUKeT4C4gEQ81DkPH1lIrFFNp2WRv9x2O5NMxdGyLRCBPva19qdYhbw0am9sR+MX4GOvn9BTky7hF0BtXUSzDrSDJCxpIS9L/qlSXGprVFi+2UKfHEj2B+HZ3wSprkK/lDuWWfnJxql162hulGeh6tbb2A1mVjucGHwV/rgPXzU+GhnaiVv2jhVZnPs3gufvRkApweUAUctJVPTmDAoik/05NTbQj5Y9KPcEHfyEpuAy6TDZ6pBxOXc2QlgYf6tE1aOZt+xy9K9MmriMFTDdGNmGt7x3hO16qBEb8lWhzbH7D05hwsPTQTnbk37lG81wc05zx1MUKLwOW1g6oozfI1A8bFqpsJQfsS+ZXKqyTeKrlqFg2n3V0JsYqtwg1po4J0RTxakGdz9WuduRlTq4MSt6+9Jt6DNhAoQ1WdTAXv8GVslVz7/OkzWt2XdN01QhKhYU9AOQHzxcEbH3QM7zrbWv5T+ndIfCmSo29XDuxW9GvAaXvlutSH1w1aZNzjNy6ZSrqn+ETgsT2vwiRxOGk26Lo31Vpp4AoXKxEW3/Ty9NaTTmKnMJ4WIfqQfWeLR4hXysptmN2IoDwPwLfAE77xKQWpbHBgBxMTZcxtE4v5tgmdiu0m/hRTlwuDgTO1O/f/q595ty9wsF/vWPt6Q84tpbRB6PvaBzoMaYaj5HtGGIYffmCPFfHlbF4ljAdFzcS9hsB718gTN2obtX98SnPl/PuvzQjD55DFVIBBovz1TXmxArF9gGRGlcmzIcaJ3aqP71zBcUhrKrmvgSK9RcC00WBhrmEaDNoxHm0gn1h26mi+7a0i7ApGzH3tzNoVMZARJEpTL3z1ZNMTMQnfVWCgKNuuqsCOgdxVTrriN9uHBm1q6qv3TsVqdiCnhnq0wf2Ct5RK/+64y8AI8ByqT8dwJT2DYPTJjAzfq34jjXACDCyxjtpqN6DPr9dF7gmy0HCEmwby5UqL9+uYBTAxerwzn58ilEGZWgAEgR5d42Y/94x8qge7Pbv4gUpaySgZkRBL+ku+h//k5YfVZ4fwNA/Mf4fRdcmuiG4xnXdKXzXomrKddiQp7u5dInBmUp2P+F2DxgVQY21qQDtYDbB4KWNSR39+RJUEjFoMbHyAYUT/nMaQWjKctHJxCoAz2jCo8gA==",
  sources: [ {type: "video/webm", src: "tiddlypwa-demo.webm"} ]
}, inFeed) }}

[TiddlyPWA](https://tiddly.packett.cool/).

It's still rough around the edges, but I've put *a lot* of effort into the setup experience, hiding all the complexity of the flexibility available with how the system is structured
and presenting a very simple "happy path" where if you open the app from a sync server it's already preconfigured to sync with that server and so on.

I've also tried to document it, though I have to say that detailing the security model was a lot easier than trying to explain the whole synchronization/server/content-versus-app thing.
Hopefully at least for those familiar with TiddlyWiki it's not too hard to understand that there's the "app wiki" with the core/plugins/themes and the separate encrypted content,
and that the sync server can work with both (differently).

Now, to justify the whole existence of this blog post, let me also list some random fun facts and whatnot – everything that didn't fit in the documentation itself :)

## Random Development Tidbits (ha)

Because of how browsers work, I've had to take extra care to make the storage work across multiple running tabs without corrupting anything.
Moreover, I made it all explicitly work together well and it's kind of a hidden feature now.
Thanks to a combination of BroadcastChannels and Web Locks, you can enjoy a multi-tab experience.
Browse the same wiki simultaneously in two tabs, the navigation will be independent but any changes will be visible everywhere.

This whole [argon2ian](https://codeberg.org/valpackett/argon2ian) thing you might've seen was indeed created for TiddlyPWA!
I've started out using PBKDF2 because it's available in the Web Crypto API, but eventually decided to upgrade to a modern password hash rather than cranking out iteration counts.
I wasn't satisfied with the state of existing Argon2 WASM+JS wrappers, so I set out to make my own, code-golfed to the tiniest size possible.
The [yak shaving stack](https://blahaj.zone/notes/9g8r2ffu4o) got even deeper during that subproject.
Also, I have used the very new [Compression Streams API](https://developer.mozilla.org/en-US/docs/Web/API/Compression_Streams_API) to be able to bundle the Wasm binary as compressed while not having to bunlde a decompressor.
And this has led to the creation of a very funny bug…

…so since that API was already used there I started using it to compress long-enough tiddler contents as well.
Stream APIs are kind of annoying when you don't want to stream, so I went with the whole "a stream is an async iterator" thing.
When I first gave TiddlyPWA to an external tester I got a report of not being able to save a particular tiddler.
Turns out, that iterator thing is only available in Firefox as of right now.
I accidentally made it so that [*tiddlers longer than 240 bytes would only work in Firefox*](https://blahaj.zone/notes/9gv2anuoss). :D

Another [really funny bug](https://blahaj.zone/notes/9gjs72259o) is something I [bugzilled here](https://bugzilla.mozilla.org/show_bug.cgi?id=1840934) for Firefox Android.
When using [FlorisBoard](https://github.com/florisboard/florisboard), pressing "enter" would result in the password mask (bullet ••• characters) becoming the actual value of the input.
This is something that I have discovered while typing passwords into my TiddlyPWA password prompt!
I also ran into an [already reported](https://github.com/mozilla-mobile/firefox-ios/issues/15244) `prompt()` bug in Firefox on iOS.

The server was initially written with SQLite as it is now, but when Deno KV was announced I got excited and rewrote it to use KV, hoping to leverage Deno Deploy for an easy server hosting experience.
I quickly ran into a 64KiB limit per entry when trying to do the whole "save the HTML page" thing, but I found [kv-toolbox](https://github.com/kitsonk/kv-toolbox), a module that would do chunking for me.
Then, after everything was ready, already with the aforementioned other person testing it, I discovered (and [ranted about](https://blahaj.zone/notes/9h1dr9g4jq)) the undocumented [10 mutations in a transaction](https://github.com/denoland/deno/issues/19284) limitation.
I wasn't about to give up on atomicity, so I just rewrote everything back to SQLite in a burst of anger and now I hold a bit of a grudge against Deno KV >_<
Even though I recognize that, obviously, it's just that *my use case is absolutely not the intended one*.

[This TypeScript thing](https://stackoverflow.com/a/68695508) that I [referenced on fedi](https://blahaj.zone/notes/9h1fdi76dt) was actually for the SQLite part of the TiddlyPWA server.

There was a [very last minute](https://blahaj.zone/notes/9hlma651nz) backwards-incompatible change I suddenly managed to do right before release.

## So, TiddlyPWA Needs YOU!

I probably could've tried to make it as a commercial product with a centrally hosted server, but that's not what I made.
TiddlyPWA is fully free (both "as in beer" and "as in freedom") because I believe in software that empowers everyone to own their data *and the software itself*, that ~~is never gonna give you up~~ doesn't die for business reasons.

Right now TiddlyPWA is still early in its development, still missing a lot of things, but hopefully is solid enough to make you check it out.
If you like what you see, please consider helping me continue working on it:

* go ahead and [try TiddlyPWA](https://tiddly.packett.cool/) first of course!
* report issues, read the code and suggest code changes [on Codeberg](https://codeberg.org/valpackett/tiddlypwa);
* support me [on Patreon](https://www.patreon.com/valpackett) :3 I'm trying to do this open source thing full time, which is only possible with your support!
* follow me [on the fediverse](https://blahaj.zone/@valpackett);
* share TiddlyPWA with anyone who might be interested.

Thanks!!
