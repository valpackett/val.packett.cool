---
title: Contributions to the Wayland ecosystem
shorttitle: Wayland
desc: Ported compositors to FreeBSD, worked on Wayfire, improved Wayland support in apps
icon: M12 0a12 12 0 0 0-5.6 1.42h2.32v.16c-.32.01-.53.1-.65.25a1.25 1.25 0 0 0-.7-.25h-.79l-.44.07-.06-.06a12 12 0 0 0-.93.59 7 7 0 0 1 1.2 2.48H7v.15h-.5V5c.2.7.34 1.04.43 1.04h.15l.34-.08.08.08.08-.08.07.08v.07a1.4 1.4 0 0 0-.51.76h-.06v.16h.13c.18 2.53.51 4.45 1 5.76l.28.23h.16c.19 0 .59-.9 1.2-2.67.4-.8.68-1.51.86-2.12l-.08-.25c.4-.59.8-.89 1.23-.89h.13c.27 0 .46.17.57.5.36.5.65 1.26.86 2.26l1.07 2.37c.14.17.21.36.21.57.4.9.75 1.49 1.07 1.78H16c.27 0 .62-1.11 1.07-3.33.08 0 .29-.6.63-1.78v-.09l-1.2-.16-.29.1h-.36v-.19l.72-.07H18a39.2 39.2 0 0 1 1.5-4.55c.05-.69.1-1.25.16-1.72a12 12 0 0 0-2.75-1.73c-.09.18-.24.28-.48.28v-.16l.43-.14A12 12 0 0 0 12 0zM9.71 1.42h1.93v.16l-.43.07-.42-.07-.37.16-.13-.16H9.7v-.16zm4 1.3h.2v.07c0 .1-.06.16-.2.16l-.08.09v.09l-.2-.1v-.15l.28-.16zm-1.07.89v.09c0 .12-.3.25-.92.39v-.16l.42-.23.08.07h.06l.36-.16zm-9.2.02A12 12 0 0 0 0 12a12 12 0 0 0 12 12 12 12 0 0 0 12-12 12 12 0 0 0-2.38-7.12 6.4 6.4 0 0 0-.92 2.15c-.14 1.1-.38 2.13-.7 3.07-.3.97-.45 1.87-.45 2.7L19 14.4c0 .14.2.28.63.42-.08.32-.2.48-.34.48v.09h.13v.07c0 .16-.22.25-.65.25l.08.09v.32h-.14c.1.32.28.48.58.48v.16h-.58v.09c.2 0 .29.13.29.4l-.8.33.07.09v.07c0 .1-.1.15-.28.15v.1c.24 0 .36.05.36.16-.34.13-.8.8-1.36 2.02-.57 1.08-1.07 1.62-1.49 1.62h-.15c-.32-.24-.58-1.05-.78-2.41-.82-2.08-1.22-3.3-1.22-3.67a6.9 6.9 0 0 0-.79-3.01 8.47 8.47 0 0 1-.28-1.78l-.35-.09c-.86 1.56-1.32 2.79-1.36 3.67a7.97 7.97 0 0 0-.57 1.87c.52.17.79.33.79.48v.09c-.41-.06-.7-.18-.86-.34-.18.4-.42 1.55-.71 3.41-.23.87-.52 1.3-.86 1.3a5.5 5.5 0 0 1-2.43-1.86c-.1 0-.27-.44-.5-1.3-.13 0-.23-.44-.28-1.3a16.6 16.6 0 0 1-1-4.22v-2.37h-.07l-.34.1v-.1c0-.1.11-.16.34-.16L4 8.73V7.66l-.55-3.41v-.63zm7.13.8v.07c-.26.24-.7.46-1.3.66V5c.1 0 .16-.09.16-.25l1.14-.32zm10.81.13-.03.25h.13l.06-.04a12 12 0 0 0-.16-.21zm-7.17.66.07.1v.06l-.13.19h-.08l-.15-.19v-.06l.29-.1zm2.63.1h.08v.06l-.14.19h-.07v-.1l.13-.15zm-1.2 1.14.28.22c-.05.17-.19.25-.42.25l-.15-.25c0-.07.1-.15.29-.22zm1.72 1.04.05.1v.06l-.05.1h-.16v-.1l.16-.16zm-2.72.82v.07l-.36.34h-.21v-.09c0-.13.2-.24.57-.32zM2.8 11.5h.08v.07c-.2.5-.36.8-.5.91h-.15v-.34l.57-.64zm9.34 3.8h.08v.17l-.58.34-.07-.1v-.15l.57-.25zM4.3 16.38h.13l.23.89H4.5l-.2-.82v-.07zm7.64 1.05H12l.23.25-.08.07h-.08l-.13-.23v-.1z
contrib:
  order: 1
---

The [Wayland](https://wayland.freedesktop.org/) display server protocol has been one of my interests since I've been able to run a compositor on [FreeBSD](/contrib/freebsd/) for the first time.
I have contributed a bunch of little things around the whole ecosystem.

## FreeBSD porting

As I've been using [FreeBSD](/contrib/freebsd/) on the desktop, I've been involved in Wayland porting early on.

Back in late 2016 I started with [Sway](https://github.com/swaywm/sway/pull/985) which was still based on [wlc](https://github.com/Cloudef/wlc/pull/211) then.
Later I maintained a [fork of Weston](https://github.com/valpackett/weston) with FreeBSD support and a bunch of other experiments.
I have landed a few Weston patches upstream since I've been finding and fixing interesting bugs by using Weston daily :)
Later, while considering various options for creating compositors, I have experimented with [making a Rust binding to libweston](https://github.com/valpackett/weston-rs).

I have been upstreaming fixes to other related libraries such as [libinput](https://gitlab.freedesktop.org/libinput/libinput/-/merge_requests/36) too.

When Sway switched to its own wlroots library, [I ported it to FreeBSD too](https://gitlab.freedesktop.org/wlroots/wlroots/-/merge_requests/252).
Other wlroots compositors started appearing soon after, and among them I found [Wayfire](https://wayfire.org/).

## Wayfire development

When I saw Wayfire for the first time, I became a big fan instantly — it was a Compiz-inspired compositor, designed to be a flexible base for desktop environments, which is exactly what I wanted.
Wayfire has definitely been one of my favorite projects to work on.

I have landed [a lot of patches](https://github.com/WayfireWM/wayfire/pulls?q=author%3Avalpackett) over the years,
starting of course with [FreeBSD build fixes](https://github.com/WayfireWM/wayfire/pull/121),
perhaps most famously adding [vswipe, the touchpad swipe gesture desktop switcher plugin](https://github.com/WayfireWM/wayfire/pull/249),
and working on other things like [various](https://github.com/WayfireWM/wayfire/pull/369) [bug](https://github.com/WayfireWM/wayfire/pull/477) [fixes](https://github.com/WayfireWM/wayfire/pull/701), tracking wlroots updates, [adding CI](https://github.com/WayfireWM/wayfire/pull/416) and so on.
I've been also involved in [the external plugin world](https://github.com/WayfireWM/wayfire/wiki/Related-projects),
perhaps most notably with the [GSettings config backend](https://codeberg.org/valpackett/wf-gsettings).

## Wayland support everywhere

I have also contributed a bunch of Wayland-related patches to various projects including:

- [Firefox](/contrib/firefox/)
- [WezTerm](https://github.com/wez/wezterm/pulls?q=is%3Apr+author%3Avalpackett)
- [RetroArch](https://github.com/libretro/RetroArch/pulls?q=is%3Apr+author%3Avalpackett)
- [FreeCAD](https://github.com/FreeCAD/FreeCAD/pull/4360) and [Coin3D](https://github.com/coin3d/coin/pull/404)
- [GLFW](https://github.com/glfw/glfw/pull/1235)
- [PPSSPP](https://github.com/hrydgard/ppsspp/pull/10449)
- [RPCS3](https://github.com/RPCS3/rpcs3/pull/3994)
